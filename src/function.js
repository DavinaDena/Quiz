let theContent = [
    {
        questionC: 'How many horses are on each team in a polo match?',
        choiceA: 'Five',
        choiceB: 'Two',
        choiceC: 'Six',
        choiceD: 'Four',
        answerC: 'D',
    },

    {
        questionC: 'What is Japanese sake made from?',
        choiceA: 'Wine',
        choiceB: 'Rice',
        choiceC: 'Water',
        choiceD: 'Coca-cola',
        answerC: 'B',
    },


    {
        questionC: 'What is the capital of Argentina?',
        choiceA: 'Buenos Aires',
        choiceB: 'Canberra',
        choiceC: 'Paris',
        choiceD: 'Rio De Janeiro',
        answerC: 'A',
    },

    {
        questionC: 'What are the five colors of the Olympic ring?',
        choiceA: 'Blue, yellow, pink, green and red',
        choiceB: 'Blue, yellow, violet, green and red',
        choiceC: 'Blue, yellow, pink, green, grey and red',
        choiceD: 'Blue, yellow, black, green and red',
        answerC: 'D',
    },

    {
        questionC: 'What was The First Home Console?',
        choiceA: 'Atari 2600',
        choiceB: 'NES',
        choiceC: 'Oddessy',
        choiceD: 'Sega Genisis',
        answerC: 'C',
    },

    {
        questionC: 'Which popular video game franchise has released games with the subtitles World At War and Black Ops?',
        choiceA: 'Call of Duty ',
        choiceB: 'Medal of Honor',
        choiceC: 'Half-Life',
        choiceD: 'Counter-Strike',
        answerC: 'A',
    },

    {
        questionC: 'What is the capital city of Australia',
        choiceA: 'Sofia',
        choiceB: 'Canberra',
        choiceC: 'Brussels',
        choiceD: 'Ottawa',
        answerC: 'B',
    },

    {
        questionC: 'Which planet has the most moons?',
        choiceA: 'Uranus',
        choiceB: 'Jupiter',
        choiceC: 'Saturn',
        choiceD: 'Earth',
        answerC: 'B',
    },
    {
        questionC: 'What is the smallest planet in our solar system?',
        choiceA: 'Neptune',
        choiceB: 'Venus',
        choiceC: 'Mars',
        choiceD: 'Mercury',
        answerC: 'D',
    },
    {
        questionC: 'Where was Frida Kahlo born?',
        choiceA: 'Mexico ',
        choiceB: 'Spain',
        choiceC: 'Argentina',
        choiceD: 'Colombia',
        answerC: 'A',
    },
    {
        questionC: 'Which legendary surrealist artist is famous for painting melting clocks?',
        choiceA: 'Max Ernst  ',
        choiceB: 'Yves Tanguy',
        choiceC: 'Salvador Dali',
        choiceD: 'René Magritte',
        answerC: 'C',
    },
    {
        questionC: 'What is the name of The Bride, played by Uma Thurman, in Quentin Tarantino’s Kill Bill franchise? ',
        choiceA: 'O-Ren Ishii',
        choiceB: 'Beatrix Kiddo',
        choiceC: 'Elle Driver',
        choiceD: 'Vernita Green',
        answerC: 'B',
    },
    {
        questionC: 'Which colour pill does Neo swallow in The Matrix?',
        choiceA: 'Red',
        choiceB: 'Blue',
        choiceC: 'Green',
        choiceD: 'Yellow',
        answerC: 'A',
    },
    {
        questionC: 'In which year was the Microsoft XP operating system released?',
        choiceA: '2002',
        choiceB: '2004',
        choiceC: '2005',
        choiceD: '2001',
        answerC: 'D',
    },
    {
        questionC: 'Elon Musk is the CEO of which global brand?',
        choiceA: 'Nasa',
        choiceB: 'Apple',
        choiceC: 'Tesla',
        choiceD: 'Microsoft',
        answerC: 'C',
    },


];


let gameBox = document.querySelector('.gameBox')
let pointBox = document.querySelector('.boxPoint')
let theQuestion = document.querySelector('.gameQuestion');
let boxAnswers = document.querySelectorAll('.gameAnswers');
let finalMessage = document.querySelector('.finalMessage');
let A = document.querySelector('.A');
let B = document.querySelector('.B');
let C = document.querySelector('.C');
let D = document.querySelector('.D');
let restart= document.querySelector('.restart')
let startBtn = document.querySelector('.start');
let points = document.querySelector('.point');
let endPoints = document.querySelector('.endPoints');
let img= document.querySelector('.theImg')
const lastQuestion = theContent.length - 1;
let runningQuestion = 0;
let score = 0;
startBtn.style.display = 'block';



// function to get the questions into the html
function renderQuestion() {
    let q = theContent[runningQuestion];
    theQuestion.innerHTML = q.questionC;
    A.innerHTML = q.choiceA;
    B.innerHTML = q.choiceB;
    C.innerHTML = q.choiceC;
    D.innerHTML = q.choiceD;
}


//loop to check if the answer is correct, show score and to pass to the next question
for (let choice of boxAnswers) {
    choice.addEventListener('click', () => {
        //we have to do a loop inside a loop, the exterior one if for
        //when the questions are finished
        if (runningQuestion < theContent.length - 1) {
            //and this one inside is to increase the score and pass the next
            //question and to score
            if (choice.classList.contains(theContent[runningQuestion].answerC)) {
                score++
                points.innerHTML = score;
                choice.classList.add('correct');
                runningQuestion++;
                //renderQuestion()
                setTimeout( function() {
                    renderQuestion();
                    choice.classList.remove('correct');
            }, 2000);
            //....and whatever else you need to do
            } else {
                choice.classList.add('incorrect');
                runningQuestion++;
                //renderQuestion();
                setTimeout(function(){
                    renderQuestion();
                    choice.classList.remove('incorrect');
            }, 2000);
            }
        } else {
            //we use again the core of the score with another loop because it
            //wasnt counting the last points
            if (choice.classList.contains(theContent[runningQuestion].answerC)) {
                score++
                points.innerHTML = score;
                finalMessage.style.display = 'block';
                gameBox.style.display = 'none';
                endPoints.textContent = score;
            } else {
                finalMessage.style.display = 'block';
                gameBox.style.display = 'none';
                endPoints.textContent = score;
            }
        }
    })

}



renderQuestion()

//all i need now is a display on the button start/questions and a end page



startBtn.addEventListener('click', () => {
    start();
})

restart.addEventListener('click', () =>{
    location.reload();
})


function start(){
    if (startBtn.style.display === 'block') {
        startBtn.style.display = 'none';
        gameBox.style.display = 'block';
        img.style.display = 'block';

    }
}