## General Knowledge Quiz

For this quiz I decided to do about general knowledge.
I just made one maquette that could be functional, simple and clean, here it is my [maquette](https://www.figma.com/file/XW4TgUn6pXoaaYL3tXRQLC/QuizMaquette?node-id=0%3A1)

It was a fun project that helped a lot to understand some operations that JavaScript allowed. A lot of thanks to my classmates that helped me.


-Me trying to explain what I did, but without being sure if suit for an employer-

About the code- I tried to make it as simple as possible for a better reading, but also for future reference and also due to the help of my classmates.

I also tried to organise it in a way that is easier to understand starting by an Array that contains the questions and the answers, followed by some variables, some that I got from the HTML and some that I created and next is a function that allows to show the content of the array on the HTML.

The big fat chunk of code in the middle is the magic of the project.
I created a loop followed by a condition (the second one) that will rotate the questions and the answers and increase the points if correct or not and that passes to the next one.

But when the quiz arrived to an end, there was an error, so I created a second condition that allows it to react in function if the quiz finishes or not.

So that was the hardest part, then I finished by creating some addEvenListener for the button Start and Restart
and the final function is a simple function that hides or show the content in function of where we are/ what we click. 

And that's it! Specially thanks to my classmates!



